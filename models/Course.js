const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
			firstname: String,
			lastName: String,
			email: String,
			password: String,
			mobileNumber: Number,
			isAdmin: Boolean,
			enrollments:[
				{
					courseId: String,
					courseName: String,
					isPaid: Boolean,
					dateEnrolled: String,
				}
			]			
	});

module.exports = mongoose.model("User",userSchema);


const courseSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Course is required"]
		},
		description: {
			type: String,
			required: [true, "Description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},		
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "UserId is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
			}
		]
			
	});

module.exports = mongoose.model("Course",courseSchema)
