/*
App: Booking System API

Scenario:
	A course booking system application where a user can enroll into a course

	type: Course Booking System (Web App)

	Description:
		-A course booking application where a user can enroll into a course 
		-Allows an admin to do CRUD opereation
		-Allows users to register into our database

Features:
	-User login (user authentication)
	-User registration

	Customer/Authenticated Users:
	-View Courses (all active courses)
	-enroll courses

	Admin Users:
	-Add courses
	-update Courses
	-Archived/Unarchived a course(soft delete/reactivate the course)
	- view Courses (all courses active/inactive)


	All users(guests, customers, admin)
	-view active courses

*/

//Data Model for the Booking System

//Two-way Embedding

/*
	user{
		
			id - unique identifier for the document
			firstname,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin,
			enrollments:[
				{
					id - document identifier
					courseId - the unique identifier
					courseName - optional
					isPaid,
					dateEnrolled
				}
			]
		}

	course {
				id - unique identifier for the document
				name,
				description,
				price,
				slots,
				isActive,
				createdOn,
				enrollees: [
					{
						id - document identifier
						userId,
						isPaid,
						dateEnrolled
					}
				]
			}

*/

//REFERENCING

/*
user{
		
		id - unique identifier for the document
		firstname,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin,
	}

course {
			id - unique identifier for the document
			name,
			description,
			price,
			slots,
			isActive,
			createdOn
		}

enrollment {
				id - unique id for the document
				userId - unique identifier for the user
				courseId - unique identifier for the course
				courseName - optional
				isPaid,

			}

*/