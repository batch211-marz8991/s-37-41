const express = require("express");
const router = express.Router();

const userController = require('../controllers/userControllers')


//Route for checking if the users email already exist in the database
//pass the "body" property of our request

router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(
		resultFromController=>res.send(
			resultFromController))
})


router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(
		resultFromController=>res.send(
			resultFromController))
})

//route for user authentication


router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(
		resultFromController=>res.send(
			resultFromController))
})

router.post("/details",(req,res)=>{
	userController.getProfile(req.body).then(
		resultFromController=>res.send(
			resultFromController));
})
module.exports = router;